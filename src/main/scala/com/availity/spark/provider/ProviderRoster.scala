package com.availity.spark.provider

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
//import org.apache.spark.sql.types.{StructType, DateType, StringType}
//import org.apache.spark.sql.functions.{count, lit, array, collect_list, col, month, avg}

object ProviderRoster {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Provider Roaster")
      .master("local[*]")
      .getOrCreate()

    // Load data
    val providersDf = spark.read
      .option("header", true)
      .option("delimiter", "|")
      .csv("data/providers.csv")

    val visitsDf = spark.read
      .option("inferSchema", "true")
      .option("header", false)
      .csv("data/visits.csv")
      .toDF("unique_visit_id","provider_id","date_of_service")

    val calculateVisitsBySpecialtyDf=calculateVisitsBySpecialty(providersDf, visitsDf, spark)
    calculateVisitsBySpecialtyDf.write
      .partitionBy("provider_specialty")
      .json("output/provider_visits_by_specialty")

    val calculateVisitsByMonthDf=calculateVisitsByMonth(visitsDf, spark)
    calculateVisitsByMonthDf.write
      .json("output/visits_by_provider_month")

    spark.stop()
  }

  def calculateVisitsBySpecialty(providersDf: DataFrame, visitsDf: DataFrame, spark: SparkSession): DataFrame = {
    val providerVisits = visitsDf
      .groupBy("provider_id")
      .count()
      .withColumnRenamed("count", "total_visits")

    val resultDf = providersDf
      .join(providerVisits, "provider_id")
      .select("provider_id", "first_name", "middle_name", "last_name", "provider_specialty", "total_visits")

    resultDf
  }

  def calculateVisitsByMonth(visitsDf: DataFrame, spark: SparkSession): DataFrame = {
    import spark.implicits._

    val resultDf = visitsDf
      .withColumn("month", date_format($"date_of_service", "yyyy-MM"))
      .groupBy("provider_id", "month")
      .count()
      .withColumnRenamed("count", "total_visits")
      .orderBy("provider_id", "month")

    resultDf
  }
}
