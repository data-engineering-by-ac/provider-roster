package com.availity.spark.provider

import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funspec.AnyFunSpec
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.{StructType, StructField, IntegerType, StringType}
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.{count, date_format}

class ProviderRosterSpec extends AnyFunSpec with DataFrameComparer with BeforeAndAfterEach {

  val spark = SparkSession.builder()
    .appName("Provider Roaster Test")
    .master("local[2]")
    .getOrCreate()

  implicit class DataFrameOps(df: DataFrame) {
    def isEmpty: Boolean = df.take(1).isEmpty
  }

  override def beforeEach(): Unit = {
    spark.sparkContext.setLogLevel("ERROR")
  }

  override def afterEach(): Unit = {
      //..
  }

  describe("ProviderRoaster process") {
    import spark.implicits._
    it("should correctly join providers and visits DataFrames") {
      val providers = Seq(
        (1, "Provider A", "Specialty X"),
        (2, "Provider B", "Specialty Y")
      ).toDF("provider_ID", "name", "specialty")

      val visits = Seq(
        (100, 1, "2022-01-01"),
        (101, 1, "2022-01-02"),
        (102, 2, "2022-01-01")
      ).toDF("unique_visit_ID", "provider_ID", "date_of_service_of_the_visit")

      val expected = Seq(
        (1, "Provider A", "Specialty X", 2L),
        (2, "Provider B", "Specialty Y", 1L)
      ).toDF("provider_ID", "name", "specialty", "total_visits")

      val providerVisits = visits
        .groupBy("provider_ID")
        .count()
        .withColumnRenamed("count", "total_visits")

      val result=providers
        .join(providerVisits, "provider_ID")
        .select("provider_ID", "name", "specialty", "total_visits")

      assertSmallDataFrameEquality(result, expected, ignoreNullable = true)
    }

    it("visits data should calculate monthly visits correctly") {
      val visitsDf = spark.read
        .option("inferSchema", "true")
        .option("header", false)
        .csv("data/visits.csv")
        .toDF("unique_visit_id","provider_id","date_of_service")
        .withColumn("month", date_format($"date_of_service", "yyyy-MM"))

      val resultDf = visitsDf
        .groupBy("provider_id", "month")
        .count()

      assert(resultDf.count() > 0)
      assert(resultDf.columns.contains("month"))
      assert(resultDf.columns.contains("count"))
    }

    it("should handle empty input DataFrames") {
      val schema = StructType(Array(
        StructField("provider_ID", IntegerType, nullable = false),
        StructField("name", StringType, nullable = true),
        StructField("specialty", StringType, nullable = true)
      ))

      val emptyProviders = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schema)
      val emptyVisits = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schema) // Ensure visits have a similar schema or adjust accordingly


      val joined = emptyProviders
        .join(emptyVisits, emptyProviders("provider_ID") === emptyVisits("provider_ID"))

      assert(joined.isEmpty, "The result should be empty for empty inputs")
    }
  }
}