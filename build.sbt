ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"

lazy val root = (project in file("."))
  .settings(
    name := "Provider-Roster",
    idePackagePrefix := Some("com.availity.spark.provider")
  )

val sparkVersion="3.5.0"
// https://mvnrepository.com/artifact/org.apache.spark/spark-sql
libraryDependencies ++=Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion ,
  "org.apache.spark" %% "spark-sql" % sparkVersion ,
  "com.github.mrpowers" %% "spark-daria" % "1.2.3",
  "com.github.mrpowers" %% "spark-fast-tests" % "1.3.0" % Test,
  "org.scalatest" %% "scalatest" % "3.2.9" % Test
)

javaOptions in run ++= Seq(
  "--add-exports=java.base/sun.nio.ch=ALL-UNNAMED"
)